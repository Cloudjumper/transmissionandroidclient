package com.web.transmission.transmissionandroidclient;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;


import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;


public class TrackerData extends ActionBarActivity {


    Map<String, Object> m_data;
    List<Map<String, Object>> l_data = new ArrayList<Map<String, Object>>();
    ListView lv_main;

    final String ATTRIBUTE_NAME_ID = "id";
    final String ATTRIBUTE_NAME_NAME = "name";
    final String ATTRIBUTE_NAME_PERCENT = "percent";

    private static final String APP_SETTINGS = "settings";
    private static final String APP_IP = "ip";
    private static final String APP_PORT = "port";

    private SharedPreferences settings;

    private Timer mTimer;
    private MyTimerTask mMyTimerTask;

    private String ip = "192.168.1.64";
    private String port = "9091";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracker_data);

        settings = getSharedPreferences(APP_SETTINGS, Context.MODE_PRIVATE);
        if(settings.contains(APP_IP) & settings.contains(APP_PORT))
        {
            ip=settings.getString(APP_IP,"192.168.1.64");
            port = settings.getString(APP_PORT,"9091");
        }
        else {
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(APP_IP, ip);
            editor.putString(APP_PORT, port);
            editor.apply();
        }
        lv_main = (ListView) findViewById(R.id.lv_main);

        mTimer = new Timer();
        mMyTimerTask = new MyTimerTask();

        mTimer.schedule(mMyTimerTask, 1000, 5000);

        }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tracker_data, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {

            AsyncPost async_post = new AsyncPost();
            String s = null;
            try {
                s = (String) async_post.execute(getResources().getString(R.string.GetTest),ip,port).get();
                Log.d("result",s);
                parseJsonString(s);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return true;
        }
        else if (id == R.id.action_settings)
        {
            Intent intent = new Intent(this, IpActivity.class);
            //startActivity(intent);
            startActivityForResult(intent,1);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        Log.d("Result code", String.valueOf(resultCode));
        if (resultCode == 1)
        {
            ip = data.getStringExtra("Ip").toString();
            port = data.getStringExtra("Port").toString();
//            Log.d("Debug data",data.getStringExtra("Ip").toString());
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(APP_IP,ip);
            editor.putString(APP_PORT,port);
            editor.apply();
        }
    }

    public void parseJsonString(String string) throws JSONException {
        JSONObject json = new JSONObject(string);
        JSONObject json_arguments = json.getJSONObject("arguments");
        JSONArray json_torrents = json_arguments.getJSONArray("torrents");
        l_data.clear();
        Log.d("Debug JSON", String.valueOf(json_torrents.length()));

        for (int i = 0; i < json_torrents.length(); i++) {
            Log.d("Debug i", String.valueOf(i));
            JSONObject json_string=(JSONObject)json_torrents.get(i);
            m_data = new HashMap<String, Object>();
            //m_data.put("id", json_string.getString("id"));
            m_data.put("name",json_string.getString("name"));
            m_data.put("percent", "Percents complete: " + String.valueOf(Double.parseDouble(json_string.getString("percentDone")) * 100) + "%");
            l_data.add(m_data);
            Log.d("Debug MAP", m_data.toString());
//            Log.d("Debug LIST", l_data.get(0).toString());
        }
//        Log.d("Debug MAP",m_data.toString());
//        Log.d("Debug JSON", m_data.get(1).get(0).toString());
//        Log.d("Debug LIST", l_data.get(0).toString());
//        Log.d("Debug LIST", l_data.get(1).toString());
        String[] from = { ATTRIBUTE_NAME_NAME,
                ATTRIBUTE_NAME_PERCENT };
        SimpleAdapter adapter = new SimpleAdapter(this,l_data,R.layout.listview_custom_1,from,
                new int[]{R.id.Tv_Text1, R.id.Tv_Text3});

        lv_main.setAdapter(adapter);
    }

    class MyTimerTask extends TimerTask {

        @Override
        public void run() {


            runOnUiThread(new Runnable() {
                String s = null;
                @Override
                public void run() {
                    AsyncPost async_post = new AsyncPost();
                    try {
                        Log.d("IP",ip);
                        Log.d("Port",port);
                        s = (String) async_post.execute(getResources().getString(R.string.GetTest),ip,port).get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                    try {
                        parseJsonString(s);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
