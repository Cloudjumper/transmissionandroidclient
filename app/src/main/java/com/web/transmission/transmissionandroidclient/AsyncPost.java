package com.web.transmission.transmissionandroidclient;

import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by nightfury on 27.05.15.
 */
public class AsyncPost extends AsyncTask {

    private String SessionId = null;
    @Override
    protected Object doInBackground(Object[] params) {

        try {
            return Get((String) params[0],(String) params[1], (String) params[2]);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    protected   String  Get(String json, String ip, String port)  throws UnsupportedEncodingException
    {
        HttpClient httpClient = new DefaultHttpClient();
        // replace with your url
        HttpPost httpPost = new HttpPost("http://" + ip + ":" + port + "/transmission/rpc/");


        //Post Data
        List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);
//        nameValuePair.add(new BasicNameValuePair("username", "test_user"));
//        nameValuePair.add(new BasicNameValuePair("password", "123456789"));



        //Encoding POST data
        try {
            StringEntity params = new StringEntity(json);
            httpPost.setEntity(params);
            httpPost.setHeader("content-type", "application/json");
            httpPost.setHeader("X-Transmission-Session-Id", SessionId);
        } catch (UnsupportedEncodingException e) {
            // log exception
            e.printStackTrace();
        }

        //making POST request.
        try {
            HttpResponse response = httpClient.execute(httpPost);
            // write response to log
            Log.d("Http Post Response:", response.toString());

            Log.d("Response Code","Response Code : "
                    + response.getStatusLine().getStatusCode());
            // create bufferreader from stream reader to read content, and more
            // string buffer...
            BufferedReader rd = new BufferedReader(new InputStreamReader(response
                    .getEntity().getContent()));
            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            // if get code 409, parse response and get new session id...
            if (response.getStatusLine().getStatusCode() == 409) {
                Pattern p = Pattern.compile("[A-z0-9]{48}");
                Matcher m = p.matcher(result);
                if (m.find()) {
                    SessionId = m.group();
                    // ...and start this function again, but with good session id
                    return Get(json, ip, port);

                } else {
                    return "Did not find session id";
                }
            } else if (response.getStatusLine().getStatusCode() == 200) {
                // after read close everything
//                httpClient.close();
                // and return data
                return result.toString();
            }
            else
                return "Something goes wrong";

        } catch (ClientProtocolException e) {
            // Log exception
            e.printStackTrace();
        } catch (IOException e) {
            // Log exception
            e.printStackTrace();
        }
        return null;
    }
}
