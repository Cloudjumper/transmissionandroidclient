package com.web.transmission.transmissionandroidclient;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class IpActivity extends ActionBarActivity {

    Button b_done;
    EditText et_ip;
    EditText et_port;

    private static final String APP_SETTINGS = "settings";
    private static final String APP_IP = "ip";
    private static final String APP_PORT = "port";

    private SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ip);
        b_done = (Button) findViewById(R.id.B_Done);
        et_ip = (EditText) findViewById(R.id.Et_Ip);
        et_port = (EditText) findViewById(R.id.Et_Port);

        settings = getSharedPreferences(APP_SETTINGS, Context.MODE_PRIVATE);
        if(settings.contains(APP_IP) & settings.contains(APP_PORT))
        {
            et_ip.setText(settings.getString(APP_IP,"192.168.1.64"));
            et_port.setText( settings.getString(APP_PORT,"9091"));
        }

        View.OnClickListener ocl_done = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent parent = new Intent();
                parent.putExtra("Ip",et_ip.getText().toString());
                parent.putExtra("Port",et_port.getText().toString());
                setResult(1, parent);
                Log.d("Debug Button", "Click");
                finish();
            }
        };
        b_done.setOnClickListener(ocl_done);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ip, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
